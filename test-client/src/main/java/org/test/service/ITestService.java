package org.test.service;

import org.test.entity.Test;

import com.baomidou.mybatisplus.extension.service.IService; 

/**
 * <p>
 * 功能 服务类
 * </p>
 *
 * @author Funkye
 * @since 2019-04-10
 */
public interface ITestService extends IService<Test> {

}
